module gitlab.com/project_falcon/kubeless/klib/podlego

go 1.14

require (
	gitlab.com/project_falcon/kubeless/klib/job v0.4.0
	gitlab.com/project_falcon/kubeless/klib/toolslego v0.24.8
	gitlab.com/project_falcon/kubeless/lib/payload v1.51.11
	k8s.io/api v0.19.4
	k8s.io/apimachinery v0.19.4
)

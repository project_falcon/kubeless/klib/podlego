package podlego

import (
	"encoding/json"
	"fmt"
	"strings"

	"gitlab.com/project_falcon/kubeless/klib/job"
	"gitlab.com/project_falcon/kubeless/klib/toolslego"
	"gitlab.com/project_falcon/kubeless/lib/payload"
	apiV1 "k8s.io/api/core/v1"
	resource "k8s.io/apimachinery/pkg/api/resource"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	curlVersion = "7.69.1"
	service     = "podlego"
)

// getLastContainerBuild this pod is added to the end of build to inform about build was finished successfully
func getLastContainerBuild(apiEvent *payload.APIData, box string, apiHost string) apiV1.Container {

	container := apiV1.Container{
		Name:    "finish",
		Image:   fmt.Sprintf("%v:%v", "curlimages/curl", curlVersion),
		Command: []string{"/bin/sh", "-c"},
		Args: []string{
			fmt.Sprintf("curl -v -d '{\"action\":\"%v\",\"idtask\":\"%v\",\"cluster\":{\"name\":\"%v\",\"type\":\"%v\",\"box\":%v}}' -H 'Content-Type:application/json' '%v'", payload.DK8sCreateDeploy, apiEvent.IDtask, apiEvent.Cluster.Name, apiEvent.Cluster.Type, box, apiHost),
		},
	}

	return container
}

func GetNodeSelector(label string, value string) map[string]string {
	m := map[string]string{
		label: value,
	}

	return m
}

// func getClusterEnvs(apiEvent *payload.APIData) []apiV1.EnvVar {
// 	// func getENVS(pData *payload.Data, yamlEnv map[string]string, stageID string, stageName string) []apiV1.EnvVar {

// 	envs := []apiV1.EnvVar{
// 		{
// 			Name:  "IDTASK",
// 			Value: apiEvent.Idtask,
// 		}, {
// 			Name:  "CLUSTER",
// 			Value: apiEvent.Cluster.Name,
// 		}, {
// 			Name:  "OWNER",
// 			Value: apiEvent.Cluster.Owner,
// 		}, {
// 			Name:  "TTL",
// 			Value: strconv.Itoa(*apiEvent.Cluster.TTL),
// 		}, {
// 			Name:  "TYPE",
// 			Value: apiEvent.Cluster.Type,
// 		},
// 	}

// if len(yamlEnv) > 0 {
// 	for envK, envV := range yamlEnv {
// 		e := apiV1.EnvVar{
// 			Name:  envK,
// 			Value: envV,
// 		}
// 		envs = append(envs, e)
// 	}
// }

// 	return envs
// }

// func getBoxEnvs(box *payload.Box) []apiV1.EnvVar {
// 	// func getENVS(pData *payload.Data, yamlEnv map[string]string, stageID string, stageName string) []apiV1.EnvVar {

// 	envs := []apiV1.EnvVar{
// 		{
// 			Name:  "BRANCH",
// 			Value: box.Branch.Name,
// 		}, {
// 			Name:  "HASH",
// 			Value: box.Branch.Hash,
// 		}, {
// 			Name:  "BRAND",
// 			Value: box.Brand,
// 		}, {
// 			Name:  "PROJECT",
// 			Value: box.Project,
// 		}, {
// 			Name:  "BASEAUTH_USER",
// 			Value: box.BaseAuth.User,
// 		}, {
// 			Name:  "BASEAUTH_PASSWORD",
// 			Value: box.BaseAuth.Password,
// 		},
// 	}

// 	// if len(yamlEnv) > 0 {
// 	// 	for envK, envV := range yamlEnv {
// 	// 		e := apiV1.EnvVar{
// 	// 			Name:  envK,
// 	// 			Value: envV,
// 	// 		}
// 	// 		envs = append(envs, e)
// 	// 	}
// 	// }

// 	return envs
// }

// ConvertStringToMap converts extraEnv to MAP
// func ConvertExtraEnvToMap(apiEvent *payload.APIData, extraEnv string) (map[string]string, error) {
// 	m := map[string]string{}

// 	err := json.Unmarshal([]byte(extraEnv), &m)
// 	if err != nil {
// 		tools.HandlerMessage(apiEvent, service, fmt.Sprintf("there is problem to UnMarshal trimEnv: '%v' to MAP. Err: '%v'", extraEnv, err.Error()), 500, tools.ErrColor)
// 		return m, err
// 	}

// 	return m, err
// }

func getBoxEnvs(apiEvent *payload.APIData, extraEnvByte []byte) ([]apiV1.EnvVar, error) {
	var envs []apiV1.EnvVar

	mapEnv, err := toolslego.ConvertExtraEnvToMap(apiEvent, extraEnvByte)
	if err != nil {
		return []apiV1.EnvVar{}, err
	}

	for key, val := range mapEnv {

		env := apiV1.EnvVar{
			Name:  key,
			Value: val,
		}

		envs = append(envs, env)
	}

	return envs, err
}

func getResources(cpu *job.CPU, mem *job.Memory, defaultSettings *job.Default) apiV1.ResourceRequirements {

	defaultSettings = defaultSettings.VerifyStruct()
	defaultSettings.Memory = defaultSettings.Memory.VerifyStruct()
	defaultSettings.CPU = defaultSettings.CPU.VerifyStruct()

	cpu = cpu.VerifyStruct()
	mem = mem.VerifyStruct()

	var cpuLimit, cpuRequest, memLimit, memRequest string

	if cpu.Limit != "" {
		cpuLimit = cpu.Limit
	} else {
		cpuLimit = defaultSettings.CPU.Limit
	}

	if cpu.Request != "" {
		cpuRequest = cpu.Request
	} else {
		cpuRequest = defaultSettings.CPU.Request
	}

	if mem.Limit != "" {
		memLimit = mem.Limit
	} else {
		memLimit = defaultSettings.Memory.Limit
	}

	if mem.Request != "" {
		memRequest = mem.Request
	} else {
		memRequest = defaultSettings.Memory.Request
	}

	cpuL, _ := resource.ParseQuantity(cpuLimit)
	cpuR, _ := resource.ParseQuantity(cpuRequest)
	memL, _ := resource.ParseQuantity(memLimit)
	memR, _ := resource.ParseQuantity(memRequest)

	resource := apiV1.ResourceRequirements{
		Limits: apiV1.ResourceList{
			apiV1.ResourceCPU:    cpuL,
			apiV1.ResourceMemory: memL,
		},
		Requests: apiV1.ResourceList{
			apiV1.ResourceCPU:    cpuR,
			apiV1.ResourceMemory: memR,
		},
	}

	return resource
}

func getVolumenEmpty() apiV1.VolumeSource {
	volumenEmpty := apiV1.VolumeSource{
		EmptyDir: &apiV1.EmptyDirVolumeSource{},
	}
	return volumenEmpty
}

func getVolumenConfigMap(name string, vol job.VolumenConfigMap) apiV1.VolumeSource {

	volumenConfig := apiV1.VolumeSource{
		ConfigMap: &apiV1.ConfigMapVolumeSource{
			LocalObjectReference: apiV1.LocalObjectReference{
				Name: name,
			},
			Items: []apiV1.KeyToPath{
				{
					Key:  vol.Key,
					Path: vol.Path,
				},
			},
			// DefaultMode: toolslego.Int32Ptr(*vol.Mode),
		},
	}

	return volumenConfig
}

func getVolumenSecret(name string, vol job.VolumenSecret) apiV1.VolumeSource {

	volumenConfig := apiV1.VolumeSource{
		Secret: &apiV1.SecretVolumeSource{
			SecretName: vol.Name,
		},
	}

	return volumenConfig
}

func replaceVAR(original string, box *payload.Box, kconfig *toolslego.KConfig, idtask string) string {
	// var newString string

	newString := strings.ReplaceAll(original, "%BRANCH%", box.Branch.Name)
	newString = strings.ReplaceAll(newString, "%HASH%", box.Branch.Hash)
	newString = strings.ReplaceAll(newString, "%PROJECT%", box.Project)
	newString = strings.ReplaceAll(newString, "%BRAND%", box.Brand)
	newString = strings.ReplaceAll(newString, "%ECRURL%", kconfig.EcrURL)
	newString = strings.ReplaceAll(newString, "%GITREPO%", kconfig.GitRepo)
	newString = strings.ReplaceAll(newString, "%IDTASK%", idtask)

	return newString
}

func getPorts(ports map[string]int32) []apiV1.ContainerPort {
	portsDefinition := []apiV1.ContainerPort{}

	if len(ports) > 0 {
		for name, number := range ports {
			port := apiV1.ContainerPort{
				Name:          name,
				ContainerPort: number,
			}

			portsDefinition = append(portsDefinition, port)
		}
	}

	return portsDefinition
}

func findEnv(arg string, envs []apiV1.EnvVar) string {
	for _, val := range envs {
		key := fmt.Sprintf("%%%v%%", val.Name)
		arg = strings.ReplaceAll(arg, key, val.Value)
	}

	return arg
}

func getArgs(argsList []string, box *payload.Box, kconfig *toolslego.KConfig, envs []apiV1.EnvVar, idtask string) []string {
	var newArgs []string

	for _, a := range argsList {
		arg := replaceVAR(a, box, kconfig, idtask)
		arg = findEnv(arg, envs)
		newArgs = append(newArgs, arg)
	}

	return newArgs
}

func getImage(image string, box *payload.Box, kconfig *toolslego.KConfig, idtask string) string {

	newImage := replaceVAR(image, box, kconfig, idtask)

	return newImage
}

func GetContainers(apiEvent *payload.APIData, containers map[int]job.Container, kconfig *toolslego.KConfig, box *payload.Box, defaultSettings *job.Default) ([]apiV1.Container, error) {

	k8sContainers := []apiV1.Container{}

	envs, err := getBoxEnvs(apiEvent, box.ExtraEnvByte)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(containers); i++ {
		c := containers[i+1]

		con := apiV1.Container{
			Name:            c.Name,
			Image:           getImage(c.Image, box, kconfig, apiEvent.IDtask),
			WorkingDir:      c.WorkingDir,
			ImagePullPolicy: apiV1.PullAlways,
			Command:         c.Commands,
			Args:            getArgs(c.Args, box, kconfig, envs, apiEvent.IDtask),
			Env:             envs,
			Resources:       getResources(c.CPU, c.Memory, defaultSettings),
			Ports:           getPorts(c.Ports),
		}

		voluments := []apiV1.VolumeMount{}

		if len(c.Vol) > 0 {
			for volName, volMount := range c.Vol {
				vol := apiV1.VolumeMount{
					Name:      volName,
					MountPath: volMount,
				}

				voluments = append(voluments, vol)
			}
		}

		con.VolumeMounts = voluments

		k8sContainers = append(k8sContainers, con)

	}

	return k8sContainers, err
}

func GetVolumens(build *job.BuildConfig) []apiV1.Volume {

	// func getVolumens(build job.BuildConfig) []apiV1.Volume {
	var volumens []apiV1.Volume

	build = build.VerifyStruct()

	build.Vol = build.Vol.VerifyStruct()

	for _, emptydir := range build.Vol.EmptyDir {
		var v apiV1.Volume

		v.Name = emptydir
		v.VolumeSource = getVolumenEmpty()

		volumens = append(volumens, v)
	}

	for key, configMap := range build.Vol.ConfigMap {
		var v apiV1.Volume

		v.Name = key
		v.VolumeSource = getVolumenConfigMap(key, configMap)

		volumens = append(volumens, v)
	}

	for key, secret := range build.Vol.Secret {
		var v apiV1.Volume

		v.Name = key
		v.VolumeSource = getVolumenSecret(key, secret)

		volumens = append(volumens, v)
	}

	return volumens
}

func GetBuild(apiEvent *payload.APIData, kconfig *toolslego.KConfig, jobDef *job.YamlRoot, box *payload.Box) (*apiV1.Pod, error) {

	podName := toolslego.GetBaseName(apiEvent.Action, box.Project)
	buildMap := make(map[string]string)
	buildMap["type"] = "build"

	initContainers, err := GetContainers(apiEvent, jobDef.Fc2.Build.Init, kconfig, box, jobDef.Fc2.Kbox.Default)
	if err != nil {
		return nil, err
	}

	// container, err := GetContainers(apiEvent, jobDef.Fc2.Build.Run, kconfig, box, jobDef.Fc2.Kbox.Default)
	// if err != nil {
	// 	return nil, err
	// }
	boxEncode, err := json.Marshal(box)
	if err != nil {
		return &apiV1.Pod{}, err
	}

	container := []apiV1.Container{getLastContainerBuild(apiEvent, string(boxEncode), kconfig.ApiServer)}

	buildDefinition := apiV1.Pod{
		ObjectMeta: metaV1.ObjectMeta{
			Name: podName,
			// Labels:  toolslego.GetBoxLabels(box, podName),
			Labels: toolslego.MergeMaps(toolslego.GetBoxLabels(*box, podName),buildMap),
			// Labels: toolslego.MergeMaps(toolslego.GetBoxLabels(*box, podName), toolslego.GetClusterLabels(apiEvent)),

			// Namespace: , we don't need to setup, because interface is already initializaed with namespace
		},

		Spec: apiV1.PodSpec{
			RestartPolicy:  apiV1.RestartPolicyNever,
			InitContainers: initContainers,
			Containers:     container,
			NodeSelector:   GetNodeSelector("app", "build"),
			Volumes:        GetVolumens(jobDef.Fc2.Build),
		},
	}

	return &buildDefinition, nil
}